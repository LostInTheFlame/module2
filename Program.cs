using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;

namespace Module2
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }

        public int GetTotalTax(int companiesNumber, int tax, int companyRevenue)
        {
            double result = companyRevenue * tax / 100d * companiesNumber;
            return Convert.ToInt32(result);
        }

        public string GetCongratulation(int input)
        {
            if (input % 2 == 0 && input >= 18)
            {
                return "Поздравляю с совершеннолетием!";
            }

            if (input % 2 != 0 && input < 18 && input > 12)
            {
                return "Поздравляю с переходом в старшую школу!";
            }

            return $"Поздравляю с {input}-летием!";
        }

        public double GetMultipliedNumbers(string first, string second)
        {
            string separator = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
            var replacement = new Dictionary<string, string> 
            {
                {".", "," },
                {",", "." },
            };
            first = first.Replace(replacement[separator], separator);
            second = second.Replace(replacement[separator], separator);
            return double.Parse(first) * double.Parse(second);
        }

        public double GetFigureValues(FigureEnum figureType, ParameterEnum parameterToCompute, Dimensions dimensions)
        {
            double output = 0;

            switch (figureType)
            {
                case FigureEnum.Triangle:
                    switch (parameterToCompute)
                    {
                        case ParameterEnum.Perimeter:
                            output = dimensions.FirstSide + dimensions.SecondSide + dimensions.ThirdSide;
                            break;
                        case ParameterEnum.Square:
                            if(dimensions.Height != 0 && dimensions.SecondSide == 0 && dimensions.ThirdSide == 0)
                            {
                                output = dimensions.FirstSide * dimensions.Height / 2d;
                                return Math.Floor(output);
                            }
                            else
                            {
                                double p = (dimensions.FirstSide + dimensions.SecondSide + dimensions.ThirdSide) / 2d;
                                output = Math.Sqrt(p * (p - dimensions.FirstSide) * (p - dimensions.SecondSide) * (p - dimensions.ThirdSide));
                                return Math.Floor(output);
                            }
                            break;
                    }
                    break;

                case FigureEnum.Rectangle:
                    switch (parameterToCompute)
                    {
                        case ParameterEnum.Perimeter:
                            output = (dimensions.FirstSide + dimensions.SecondSide) * 2;
                            break;
                        case ParameterEnum.Square:
                            output = dimensions.FirstSide * dimensions.SecondSide;
                            break;
                    }
                    break;

                case FigureEnum.Circle:
                    switch (parameterToCompute)
                    {
                        case ParameterEnum.Perimeter:
                            output = 2 * Math.PI * dimensions.Radius;
                            break;
                        case ParameterEnum.Square:
                            output = Math.PI * Math.Pow(dimensions.Radius, 2);
                            break;
                    }
                    break;
            }
            return Math.Round(output);
        }
    }
}
